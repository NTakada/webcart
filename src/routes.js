import Home from './tamplates/Home';
import Culc from './tamplates/Culc';
import CulcLoc from './tamplates/CulcLoc';
import Wether from './tamplates/Wether';

export const routes = [
  {
    path: '/',
    component: Home
  },
  {
    path: '/culc',
    component: Culc
  },
  {
    path: '/culcLoc',
    component: CulcLoc
  },
  {
    path: '/wether',
    component: Wether
  },
  {
    path: '*',
    component: Home
  }
]
