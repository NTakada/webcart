import Vue from 'vue'

import "bootstrap/dist/css/bootstrap.css"
import "bootstrap-vue/dist/bootstrap-vue.css"

import App from './App.vue'
import moment from 'moment'
import {router} from './router'

Vue.filter(
  'formatDate', function(value) {
    if (value) {
      return moment(String(value)).format('DD-MM-YYYY, HH:mm')
    }
  }
);
Vue.filter(
  'formatDateShort', function(value) {
    if (value) {
      return moment(String(value)).format('DD-MM-YYYY')
    }
  }
);

new Vue({
  router,
  el: '#app',
  render: h => h(App)
})
