<?php

class Helper
{
    public function __construct()
    {
    }

    public function getCurrencies($url)
    {

        if ($ch = curl_init())
        {
            // Устанавливаем URL запроса
            curl_setopt($ch, CURLOPT_URL, $url);

            // При значении true CURL включает в вывод заголовки
            curl_setopt($ch, CURLOPT_HEADER, false);

            // Куда помещать результат выполнения запроса:
            //  false - в стандартный поток вывода,
            //  true - в виде возвращаемого значения функции curl_exec.
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            // Нужно явно указать, что будет POST запрос
            curl_setopt($ch, CURLOPT_POST, false);

            // Максимальное время ожидания в секундах
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);

            // Выполнение запроса
            $res = curl_exec($ch);

            // Вывести полученные данные
            $_res = json_decode($res);

            // Особождение ресурса
            @curl_close($ch);

            return $_res;
        }
    }

    public function getSelected($arrBanks, $objs)
    {
        $date = $objs->date;
        $i = 0;

        foreach ($objs->organizations as $obj){
            if(in_array($obj->id, $arrBanks)){
                $data[$i]['id'] = $obj->id;
                $data[$i]['params']['id'] = $obj->id;
                $data[$i]['params']['title'] = $obj->title;

                foreach ($obj->currencies as $name => $curr){
                    $data[$i]['params']['valutaArr'][] = $name;
                    $data[$i]['params']['valuta'][$name]['type'] = $name;
                    $data[$i]['params']['valuta'][$name]['name'] = $objs->currencies->$name;
                    $data[$i]['params']['valuta'][$name]['bid'] = $curr->bid;
                    $data[$i]['params']['valuta'][$name]['ask'] = $curr->ask;
                }

                $data[$i]['params']['date'] = $date;
                $i++;
            }
        }

        return $data;
    }

    public function putDataToFile($jsonObjs)
    {
        $file = '/home/vlad/www/simpl1.loc/uploads/test.txt';

        if(file_put_contents($file, $jsonObjs)){
            echo "Ура! Записали (somecontent) в файл ($file)";
            print_r($jsonObjs);
        } else {
            echo "Файл $file недоступен для записи";
        }
    }

    public function getValuta($objs)
    {
        return array_keys((array)$objs->currencies);
    }

    public function getCurrNames($currencies)
    {
      $currencies = (array)$currencies;
      return json_encode($currencies);
    }
}
